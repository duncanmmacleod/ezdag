# Changelog

## [Unreleased]

## [0.1.0] - 2022-01-25

- Initial release.

[unreleased]: https://git.ligo.org/patrick.godwin/ezdag/-/compare/v0.1.0...main
[0.1.0]: https://git.ligo.org/patrick.godwin/ezdag/-/tags/v0.1.0
